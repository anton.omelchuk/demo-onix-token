/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'default-layout': '#f4f6ff',
        'active': '#243c5a',
        'dark': '#24252D',
        'dark-element': '#233447',
        'gray-1': '#E3E1E3',
        'gray-3': '#4F4F4F',
        'gray-2': '#888888',
        'black-1': '#2D2E36',
        'black-2': '#1B1A21',
        'black-3': '#2A2D3A',
        'black-4': '#24252D',
        'wallet': '#131823',
        'address': '#324054',
        'address-light': '#ecf0f9'
      },
    },
    screens: {
      lg: { max: '1800px' },
      md: { max: '990px' },
      sm: { max: '600px' },
      xs: { max: '400px' },
    },
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
