import { FC } from 'react';
import Script from 'next/script';
import { Provider } from 'react-redux';
import { appWithTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { ToastContainer } from 'react-toastify';
import { ThemeProvider } from 'next-themes';
import type { AppProps } from 'next/app';
import IStaticProps from '../src/interfaces/IStaticProps';
import Navbar from '../src/layout/navbar/Navbar';
import Web3Provider from '../src/providers/Web3Provider';
import { useStore } from '../src/store/store';

import '../styles/globals.css';
import 'react-toastify/dist/ReactToastify.css';


const MyApp:FC<AppProps> = ({ Component, pageProps }) => {
  const store = useStore(pageProps.initialReduxState);

  return (
    <ThemeProvider attribute="class">
      <Provider store={store}>
        <Web3Provider>
            <div className="dark:bg-dark bg-white min-h-screen">
              <Navbar {...pageProps} />
              <div className="pt-65">
                <Component {...pageProps} />
                <ToastContainer />
              </div>
            </div>
        </Web3Provider>
       </Provider>

    <Script src="https://kit.fontawesome.com/d45b25ceeb.js" crossOrigin="anonymous" />
    </ThemeProvider>
  );
};

export async function getServerSideProps({ locale }: IStaticProps) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

export default appWithTranslation(MyApp);
