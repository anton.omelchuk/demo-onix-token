import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import HomeView from '../src/pages/Home/HomeView';
import IStaticProps from '../src/interfaces/IStaticProps';

function Home() {
  return (
    <HomeView />
  );
}

export async function getServerSideProps({ locale }: IStaticProps) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

export default Home;
