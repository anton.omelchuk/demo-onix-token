import { SWRResponse } from 'swr';
import { MetaMaskInpageProvider } from '@metamask/providers';
import { providers, Contract } from 'ethers';

// useAccount
export type UseAccountResponse = {
  connect: () => Promise<void>;
  isLoading: boolean;
  isInstalled: boolean;
  data: string | null;
};

export type AccountHookFactory = CryptoHookFactory<string, UseAccountResponse>;

export type UseAccountHook = ReturnType<AccountHookFactory>;

// useNetwork
export type UseNetworkResponse = {
  isLoading: boolean;
  isSupported: boolean;
  targetNetwork: string;
  isConnectedToNetwork: boolean;
};

export type NetworkHookFactory = CryptoHookFactory<string, UseNetworkResponse>;

export type UseNetworkHook = ReturnType<NetworkHookFactory>;

export type Web3Hooks = {
  useAccount: UseAccountHook;
  useNetwork: UseNetworkHook;
};

export interface IWeb3Provider {
  children: React.ReactNode;
}

type Nullable<T> = {
  [P in keyof T]: T[P] | null | undefined;
};

export type Web3State = {
  isLoading: boolean;
  hooks: Web3Hooks;
} & Nullable<Web3Dependencies>;

export type Web3Dependencies = {
  provider: providers.Web3Provider;
  contracts: Record<string, Contract>;
  ethereum: MetaMaskInpageProvider;
  isLoading: boolean;
};

export type CryptoHookFactory<D = any, R = any, P = any> = {
  (d: Partial<Web3Dependencies>): CryptoHandlerHook<D, R, P>
};

export type CryptoHandlerHook<D = any, R = any, P = any> = (params?: P) => CryptoSWRResponse<D, R>;

export type CryptoSWRResponse<D = any, R = any> = SWRResponse<D> & R;
