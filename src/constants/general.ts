export enum Theme {
  LIGHT = 'light',
  DARK = 'dark',
}

export const LanguagesList = [
  { label: 'EN', value: 'en' },
  { label: 'УКР', value: 'ua' },
];

export const NETWORKS: { [k: string]: string } = {
  1: 'Ethereum Main Network',
  3: 'Ropsten Test Network',
  4: 'Rinkeby Test Network',
  5: 'Goerli Test Network',
  42: 'Kovan Test Network',
  56: 'Binance Smart Chain',
  1337: 'Ganache',
  80001: 'Mumbai Test Network',
};

export enum CONTRACTS {
  ONIX_TOKEN = 'OnixToken',
  BLOG = 'Blog',
}

export const ONIX_TOKEN_ADDRESS = '0xdE8353b2Eec025aaa37843C04eE32E6946cb227F';
// export const BLOG_ADDRESS = '0xa1Fb9a1b06811BCE87E01337683D4BCA88B69FF7';
export const BLOG_ADDRESS = '0x16C4b27fe359f4721dB0C13A2d46B5B05698384e';

export const ADDRESSES = {
  'OnixToken': ONIX_TOKEN_ADDRESS,
  'Blog': BLOG_ADDRESS,
};

export const CREATE_POST_COST = '1000000000000000000'; // 1 ONIX token in wei
export const LIKE_POST_COST = '10000000000000000'; // 0.01 ONIX token in wei
