import IPost from '../components/PostsList/interfaces/IPost';
import { NETWORKS } from '../constants/general';
import { UseAccountResponse, UseNetworkResponse } from '../interfaces/IHooks';

export const getNetworkInfo = (network: UseNetworkResponse, account: UseAccountResponse) => {
  const { isLoading, isInstalled, data } = account;
  const { isLoading: isNetworkLoading, isSupported } = network;

  if (isLoading || isNetworkLoading) {
    return {
      dotColor: 'indigo',
      title: 'loading',
    };
  }

  if (!data) {
    return {
      dotColor: 'red',
      title: 'connectWallet',
    };
  }

  if (!isInstalled) {
    return {
      dotColor: 'red',
      title: 'installMetamask',
    };
  }

  if (!isSupported) {
    return {
      dotColor: 'red',
      title: 'useMumbaiTestnet',
    };
  }

  return {
    dotColor: 'green',
    title: NETWORKS[process.env.NEXT_PUBLIC_TARGET_CHAIN_ID as string],
  };
};

export const formatPostsData = (posts): Array<IPost> => {
  const data = posts.map((post) => ({
    id: post?.[0].toString(),
    author: post?.[1],
    content: post?.[2],
    likes: post?.[3].toNumber(),
  }));

  return data.reverse();
};

export const walletAddressFormater = (address: string): string => `${address.slice(0, 6)}...${address.slice(-4)}`;
