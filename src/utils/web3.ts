
import { MetaMaskInpageProvider } from '@metamask/providers';
import { Contract, ethers, providers } from 'ethers';
import { Web3Dependencies, Web3Hooks } from '../interfaces/IHooks';
import { setupHooks } from '../hooks/setupHooks';
import { ADDRESSES, CONTRACTS, ONIX_TOKEN_ADDRESS } from '../constants/general';

declare global {
  interface Window {
    ethereum: MetaMaskInpageProvider;
  }
}

type Nullable<T> = {
  [P in keyof T]: T[P] | null;
};

export type Web3State = {
  isLoading: boolean;
  hooks: Web3Hooks;
} & Nullable<Web3Dependencies>;


export const createDefaultState = () => ({
  ethereum: null,
  provider: null,
  contracts: null,
  isLoading: true,
  hooks: setupHooks({ isLoading: true } as any),
});

export const createWeb3State = ({
  ethereum, provider, contracts, isLoading,
}: Web3Dependencies) => ({
  ethereum,
  provider,
  contracts,
  isLoading,
  hooks: setupHooks({ ethereum, provider, contracts, isLoading }),
});

export const loadContract = async (
  name: CONTRACTS,  // OnixToken | Blog
  provider: providers.Web3Provider,
): Promise<Contract> => {

  const contractAddress = ADDRESSES[name];

  const artifactResponse = await fetch(`/artifacts/contracts/${name}.sol/${name}.json`);
  const Artifact = await artifactResponse.json() as Record<string, string>;

  const contract = new ethers.Contract(contractAddress, Artifact.abi, provider);

  if (contract) {
    return contract;
  }

  throw new Error(`Contract: [${name}] cannot be loaded!`);
};

export const addMumbaiNetwork = async () => {
  try {
    await window.ethereum.request({
      method: 'wallet_addEthereumChain',
      params: [{
        chainId: '0x13881',
        chainName: 'Mumbai Testnet',
        nativeCurrency: {
          name: 'MATIC',
          symbol: 'MATIC',
          decimals: 18,
        },
        rpcUrls: ['https://rpc-mumbai.maticvigil.com/'],
        blockExplorerUrls: ['https://mumbai.polygonscan.com/'],
      }],
    });
  } catch (e) {
    console.error(e);
  }
};

export const importOnixToken = async () => {
  await window.ethereum.request({
    method: 'wallet_watchAsset',
    params: {
      type: 'ERC20',
      options: {
        address: ONIX_TOKEN_ADDRESS,
        symbol: 'ONIX',
        decimals: 18,
        image: 'https://scontent.fdnk6-1.fna.fbcdn.net/v/t39.30808-1/275134463_1396046037521959_5664447390170225753_n.jpg?stp=dst-jpg_p200x200&_nc_cat=110&ccb=1-7&_nc_sid=c6021c&_nc_ohc=ZXe0__71WLcAX-i_hRz&_nc_ht=scontent.fdnk6-1.fna&oh=00_AfAcIvS2hURB1YVg3xZJY_qMpZ5RgvHR_HNNg2DXaYJuRA&oe=6425A7D4',
      },
    },
  });
};
