import { memo } from 'react';
import NetworkStatus from '../../components/NetworkStatus/NetworkStatus';
import PostTextarea from '../../components/PostTextarea/PostTextarea';
import DropdownSettings from '../../components/Dropdown/DropdownSettings';
import Logo from '../../components/Logo/Logo';
import Wallet from '../../components/Wallet/Wallet';

const Navbar = () => (
    <nav className="fixed flexBetween w-full z-10 p-4 px-12 flex-row border-b
      dark:bg-dark bg-default-layout dark:border-black-1 border-gray-1 dark:shadow-xl"
      >
      <Logo />
      <PostTextarea />

      <div className="flexEnd flex-1">
        <div className="flex-col">
          <NetworkStatus />
          <Wallet />
        </div>
        <div className="mr-4" />
        <DropdownSettings />
      </div>

    </nav>
);

export default memo(Navbar);
