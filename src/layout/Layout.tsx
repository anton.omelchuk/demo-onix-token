import { FC } from 'react';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import ILayout from './interfaces/ILayout';
import Loader from '../components/Loader/Loader';
import { useWeb3 } from '../providers/Web3Provider';
import { useAccount, useNetwork } from '../hooks';

const Layout:FC<ILayout> = ({ children }) => {
  const { t } = useTranslation('common');
  const { isLoading: isContractLoading, contracts } = useWeb3();
  const { account: { isLoading: isAccountLoading } } = useAccount();
  const { network: { isLoading: isNetworkLoading } } = useNetwork();

  const isLoading =  isContractLoading || isAccountLoading || isNetworkLoading;

  return (
    <div className="min-w-screen min-h-screen dark:bg-dark
     bg-default-layout flex items-center justify-center pt-[144px] pb-10">
      <Head>
        <title>{t('tabTitle')}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {(isLoading || !contracts) && <Loader />}
      {children}
    </div>
  );
};

export default Layout;
