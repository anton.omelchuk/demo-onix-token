import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import { IGeneral } from './interfaces/IGeneral';
import { formatPostsData } from '../../utils/general';
import { CREATE_POST_COST, LIKE_POST_COST, ONIX_TOKEN_ADDRESS } from '../../constants/general';

export const initialState: IGeneral = {
  posts: [],
  isLoading: false,
  addLikeIsLoading: false,
  createPostLoading: false,
};

export const getPostsAsync = createAsyncThunk(
  'general/getPostsAsync',
  async (contract) => {
    const posts = await contract.getAllPosts();
    const formattedData = formatPostsData(posts);

    return formattedData;
  },
);

export const createPostAsync = createAsyncThunk(
  'general/createPostAsync',
  async (payload, { dispatch }) => {
    const { contracts, text, successMessage, callback } = payload;
    try {
      const sendToken = await contracts?.onixToken.transfer(ONIX_TOKEN_ADDRESS, CREATE_POST_COST);
      await sendToken?.wait();

      const createPostTx = await contracts.blog.createPost(text);
      await createPostTx?.wait();

      toast.success(successMessage);
      dispatch(getPostsAsync(contracts.blog));
    } catch (error) {
      toast.error(error?.data?.message || error?.message || error.toString());
    } finally {
      callback();
    }
  },
);

export const addLikeAsync = createAsyncThunk(
  'general/addLikeAsync',
  async (payload, { dispatch }) => {
    const { contracts, id, callback, t } = payload;

    try {
      const sendToken = await contracts?.onixToken.transfer(ONIX_TOKEN_ADDRESS, LIKE_POST_COST);
      await sendToken?.wait();

      const likePostTx = await contracts.blog.likePost(id);
      await likePostTx?.wait();

      toast.success(t('postLiked'));
      dispatch(getPostsAsync(contracts.blog));
    } catch (error) {
      toast.error(error?.data?.message || error?.message || error.toString());
    } finally {
      callback();
    }
  },
);


export const generalSlice = createSlice({
  name: 'general',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getPostsAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getPostsAsync.fulfilled, (state, action) => {
        state.posts = action.payload;
        state.isLoading = false;
      })
      .addCase(getPostsAsync.rejected, (state) => {
        state.isLoading = false;
      })
      .addCase(addLikeAsync.pending, (state) => {
        state.addLikeIsLoading = true;
      })
      .addCase(addLikeAsync.fulfilled, (state) => {
        state.addLikeIsLoading = false;
      })
      .addCase(addLikeAsync.rejected, (state) => {
        state.addLikeIsLoading = false;
      })
      .addCase(createPostAsync.pending, (state) => {
        state.createPostLoading = true;
      })
      .addCase(createPostAsync.fulfilled, (state) => {
        state.createPostLoading = false;
      })
      .addCase(createPostAsync.rejected, (state) => {
        state.createPostLoading = false;
      });
  },
  reducers: {},
});


export default generalSlice.reducer;
