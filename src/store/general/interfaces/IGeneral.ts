import IPost from '../../../components/PostsList/interfaces/IPost';

export interface IGeneral {
  posts: Array<IPost>,
  isLoading: boolean,
  addLikeIsLoading: boolean,
  createPostLoading: boolean,
}
