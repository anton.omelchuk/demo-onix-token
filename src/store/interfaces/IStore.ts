import { IGeneral } from '../general/interfaces/IGeneral';

interface IStore {
  general: IGeneral
}

export default IStore;
