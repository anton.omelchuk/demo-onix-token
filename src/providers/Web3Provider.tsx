import { createContext, FC, useContext, useEffect, useState } from 'react';
import { ethers } from 'ethers';
import { toast } from 'react-toastify';
import { useTranslation } from 'next-i18next';
import { IWeb3Provider, Web3Dependencies, Web3State } from '../interfaces/IHooks';
import { createDefaultState, createWeb3State, loadContract } from '../utils/web3';
import { CONTRACTS } from '../constants/general';

const Web3Context = createContext<Web3State>(createDefaultState());

const Web3Provider:FC<IWeb3Provider> = ({ children }) => {
  const [web3Api, setWeb3Api] = useState<Web3State>(createDefaultState());

  const { t } = useTranslation('common');

  useEffect(() => {
    const initWeb3 = async () => {
      try {
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const onixTokenContract = await loadContract(CONTRACTS.ONIX_TOKEN, provider);
        const blogContract = await loadContract(CONTRACTS.BLOG, provider);

        const signer = provider.getSigner();
        const signedOnixTokenContract = onixTokenContract.connect(signer);
        const signedBlogContractContract = blogContract.connect(signer);

        setWeb3Api(createWeb3State({
          ethereum: window.ethereum,
          provider,
          contracts: {
            onixToken: signedOnixTokenContract,
            blog: signedBlogContractContract,
          },
          isLoading: false,
        }));
      } catch (e: any) {
        toast.error(e.message || e.data?.message || t('somethingWentWrong').toString());

        setWeb3Api((api) => createWeb3State({
          ...api as Web3Dependencies,
          isLoading: false,
        }));
      }
    };

    initWeb3();
  }, []);

  return (
    <Web3Context.Provider value={web3Api}>
      {children}
    </Web3Context.Provider>
  );
};

export function useWeb3() {
  return useContext(Web3Context);
}

export function useHooks() {
  const { hooks } = useWeb3();
  return hooks;
}

export default Web3Provider;
