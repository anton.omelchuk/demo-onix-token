import { useState } from 'react';
import { useTranslation } from 'next-i18next';
import { useTheme } from 'next-themes';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import Image from 'next/image';
import IStore from '../../store/interfaces/IStore';
import ButtonIcon from '../ButtonIcon/ButtonIcon';
import { useWeb3 } from '../../providers/Web3Provider';
import { createPostAsync } from '../../store/general/slice';
import { Theme } from '../../constants/general';
import { useAccount } from '../../hooks';

const PostTextarea = () => {
  const [text, setText] = useState<string>('');
  const { createPostLoading } = useSelector((state: IStore) => state.general);

  const { account: { data } } = useAccount();
  const { t } = useTranslation('common');
  const { theme } = useTheme();
  const { contracts } = useWeb3();
  const dispatch = useDispatch();

  const createPost = () => {
    if (!data) {
      toast.error(t('connectWallet').toString());
    } else if (!text) {
      toast.error(t('emptyPost').toString());
    } else {
      dispatch(createPostAsync({
        contracts,
        text,
        successMessage: t('postCreated'),
        callback: () => setText(''),
      }));
    }
  };

  const buttonIcon = theme === Theme.DARK ? '/images/plusIconDark.svg' : '/images/plusIcon.svg';

  return (
    <div className="flex flexBetween w-[540px] h-16 relative">
      <input
        type="text"
        placeholder={t('createPostPlaceholder')}
        maxLength={90}
        value={text}
        onChange={(e) => setText(e.target.value)}
        className="w-full border-gray-300 rounded-lg py-2 px-3
          focus:outline-none focus:ring focus:border-blue-300 pr-16" />
      <div className="absolute flex flexCenter right-0 bottom-0 top-1.5">
      <ButtonIcon
        icon={<Image src={buttonIcon} width={30} height={30} alt="plus icon" />}
        onClick={() => createPost()}
        isLoading={createPostLoading}
        disabled={text.length === 0 || text.length > 90}
      />
      </div>
  </div>

  );
};

export default PostTextarea;
