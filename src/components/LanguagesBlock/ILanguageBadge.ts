export default interface ILanguageBadge {
  label: string;
  isActive: boolean;
  onClick: () => void;
}
