import { useCallback } from 'react';
import { useRouter } from 'next/router';
import { i18n } from 'next-i18next';
import LanguageBadge from './LanguageBadge';
import { LanguagesList } from '../../constants/general';

const LanguagesBlock = () => {
  const router = useRouter();

  const changeLanguage = useCallback((language: string) => {
    i18n!.changeLanguage(language, (err) => {
      if (!err) {
        router.push(router.pathname, router.asPath, { locale: language });
      }
    });
  }, [router]);

  return (
    <>
      {LanguagesList.map(({ label, value }) => (
          <LanguageBadge
            key={value}
            label={label}
            isActive={i18n!.language === value}
            onClick={() => changeLanguage(value)}
          />
      ))}
    </>
  );
};

export default LanguagesBlock;