import { FC } from 'react';
import ILanguageBadge from './ILanguageBadge';

const LanguageBadge:FC<ILanguageBadge> = ({ label, isActive, onClick }) => (
    <button
      type="button"
      className={`dark:bg-gray-700 bg-gray-1 mr-2 px-2.5 py-0.5 rounded border-2
      hover:cursor-pointer ${isActive ? 'border-active dark:border-white' : 'border-transparent'}`}
      disabled={isActive}
      onClick={onClick}
    >
      <span className="dark:text-gray-300 text-gray-800 text-xs font-bold">
        {label}
      </span>
    </button>
);

export default LanguageBadge;