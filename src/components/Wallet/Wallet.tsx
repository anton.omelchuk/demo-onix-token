import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { useAccount } from '../../hooks';
import { walletAddressFormater } from '../../utils/general';

const Wallet = () => {
  const { t } = useTranslation();
  const { account } = useAccount();

  const { connect, isLoading, isInstalled, data } = account;

  const buttonHandler = () => {
    if (!isInstalled) {
      window.open('https://metamask.io', '_ blank');
    } else if (window?.ethereum) {
      window.ethereum.request({ method: 'eth_requestAccounts' });
    } else {
      connect();
    }
  };

  const title = isInstalled ? 'connect' : 'install';

  return (
    <div className="flexEnd mt-1">
      {data && !isLoading ? (
        <div className="flexCenter rounded-lg dark:bg-dark-element bg-white px-4 py-2">
          <span className="shadow-sm rounded-lg px-2 py-1 dark:bg-address bg-address-light
           dark:text-white text-black text-sm mr-2">
            {walletAddressFormater(data)}
          </span>
          <Image
            src="/images/metamask.svg"
            height={30}
            width={30}
            alt="metamask"
          />
        </div>
      ) : (<button
        onClick={() => buttonHandler()}
        type="button"
        className="inline-flex items-center px-3 py-1.5 border border-transparent text-xs
          font-medium rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700
          focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
      >
        {`${t(isLoading ? 'loading' : title)}`}
      </button>)}
    </div>
  );
};

export default Wallet;
