import { FC, memo } from 'react';
import Spinner from '../Spinner/Spinner';
import IButtonIcon from './interfaces/IButtonIcon';

const ButtonIcon:FC<IButtonIcon> = ({
  icon,
  disabled = false,
  isLoading = false,
  onClick,
}) => (
    <div>
      <button
        type="button"
        onClick={() => {
          if (!disabled && !isLoading) {
            onClick();
          }
        }}
        className="disabled:opacity-30 dark:hover:opacity-80 text-white
          font-bold py-2 px-4 rounded dark:active:opacity-60 active:opacity-60"
      >
        {isLoading ? <Spinner /> : icon}
      </button>
    </div>
);

export default memo(ButtonIcon);
