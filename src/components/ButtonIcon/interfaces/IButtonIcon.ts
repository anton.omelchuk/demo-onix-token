export default interface IButtonIcon {
  icon: React.ReactNode;
  disabled?: boolean;
  isLoading?: boolean;
  onClick: () => void;
}
