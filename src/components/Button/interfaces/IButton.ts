export default interface IButton {
  title: string;
  disabled?: boolean;
  isLoading?: boolean;
  onClick: () => void;
}
