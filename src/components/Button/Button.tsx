import { FC, memo } from 'react';
import IButton from './interfaces/IButton';
import Spinner from '../Spinner/Spinner';

const Button:FC<IButton> = ({
  title,
  disabled = false,
  isLoading = false,
  onClick,
}) => (
    <div>
      <button
        type="button"
        onClick={() => {
          if (!disabled && !isLoading) {
            onClick();
          }
        }}
        className="w-full dark:bg-dark-element disabled:opacity-30 bg-blue-500 hover:bg-blue-700
          dark:hover:opacity-80 text-white font-bold py-2 px-4 rounded
          dark:active:opacity-60 active:opacity-60"
      >
        {isLoading ? <Spinner /> : title}
      </button>
    </div>
);

export default memo(Button);
