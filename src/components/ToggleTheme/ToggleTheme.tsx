import { memo } from 'react';
import { useTheme } from 'next-themes';
import { Theme } from '../../constants/general';

const ThemeToggle = () => {
  const { theme, setTheme } = useTheme();

  return (
    <div className="flex flex-initial flex-row justify-end">
      <div className="flex items-center mr-2">
        <input
          type="checkbox"
          className="checkbox"
          id="checkbox"
          onChange={() => setTheme(theme === Theme.LIGHT ? Theme.DARK : Theme.LIGHT)}
        />
        <label htmlFor="checkbox" className="flexBetween w-12 h-5 bg-black rounded-2xl p-1 relative label cursor-pointer">
          <i className="fas fa-sun" />
          <i className="fas fa-moon" />
          <div className="w-4 h-4 absolute bg-white rounded-full ball"
          />
        </label>
      </div>
    </div>
  );
};

export default memo(ThemeToggle);
