import { memo, useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import { useTheme } from 'next-themes';
import Image from 'next/image';
import { Theme } from '../../constants/general';

const Logo = () => {
  const { theme } = useTheme();
  const { t } = useTranslation('common');

  const logo = useMemo(() => theme === Theme.DARK ? '/logoDark.svg' : '/logo.svg', [theme]);

  return (
    <div className="flex flex-1 flex-row justify-start">
        <div className="flexCenter md:hidden cursor-pointer">
          <Image src={`/images${logo}`} objectFit="contain" width={100} height={100} alt="logo" />
          <p className="dark:text-white text-black-1 font-semibold text-lg ml-4">{t('blog')}</p>
        </div>
        <div className="hidden md:flex">
          <Image src={`/images${logo}`} objectFit="contain" width={64} height={64} alt="logo" />
        </div>
      </div>
  );
};

export default memo(Logo);
