import { FC, useState } from 'react';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';
import IPost from './interfaces/IPost';
import Spinner from '../Spinner/Spinner';
import { useAccount } from '../../hooks';
import { useWeb3 } from '../../providers/Web3Provider';
import { walletAddressFormater } from '../../utils/general';
import { addLikeAsync } from '../../store/general/slice';

const Post:FC<IPost> = ({ author, content, likes, id }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { contracts } = useWeb3();
  const { account: { data } } = useAccount();
  const { t } = useTranslation('common');
  const dispatch = useDispatch();

  const handleLike = () => {
    if (author === data) {
      toast.error(t('youCantLikeYourPost').toString());
    } else {
      setIsLoading(true);

      dispatch(addLikeAsync({
        contracts,
        id,
        callback: () => setIsLoading(false),
        t,
      }));
    }
  };

  return (
    <div className="flex1 w-[540px] h-[100px] my-1.5 p-4 border rounded-2xl
    dark:bg-dark-element bg-white dark:border-black-1 border-gray-1"
    >
      <div className="flex flex-row justify-between">
        <div className="flex flexCenter flex-row">
          <div className="flex flexCenter">
            <div
              style={{ backgroundColor: `#${author.slice(2, 5)}` }}
              className="flex flexCenter h-16 w-16 rounded-full">
              <Image
                className="rounded-full"
                width={45}
                height={45}
                src="/images/metamaskPost.svg"
                alt="avatar"
              />
            </div>
          </div>
          <div className="flex flex-col ml-4">
            <span className="font-bold dark:text-gray-400">{walletAddressFormater(author)}</span>
            <div>
              <span className="font-poppins dark:text-gray-200 text-md">{content}</span>
            </div>
          </div>
        </div>
        <div className="flex flexCenter">
          {isLoading ? (
            <Spinner />
          ) : (
            <button
              type="button"
              onClick={handleLike}
              className="flex flex-row items-center text-sm">
              <i className="fas fa-thumbs-up" />
              <span className="ml-1 font-bold text-lg dark:text-gray-300">{likes}</span>
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Post;
