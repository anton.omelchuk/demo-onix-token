import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import IPost from './interfaces/IPost';
import Loader from '../Loader/Loader';
import Post from './Post';
import { useWeb3 } from '../../providers/Web3Provider';
import { useAccount } from '../../hooks';
import { getPostsAsync } from '../../store/general/slice';
import IStore from '../../store/interfaces/IStore';

const PostsLists = () => {
  const { contracts } = useWeb3();
  const { account: { data } } = useAccount();

  const { posts, isLoading } = useSelector((state: IStore) => state.general);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPostsAsync(contracts?.blog));
  }, [contracts, data, dispatch]);

  return (
    <div className="flex-col flexBetween min-w-full">
      {isLoading && <Loader />}
      {data && posts?.map(({ id, author, content, likes }: IPost) => (
        <Post key={id} id={id} author={author} content={content} likes={likes} />
      ))}
    </div>
  );
};

export default PostsLists;