import { useState } from 'react';
import { useTheme } from 'next-themes';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import LanguagesBlock from '../LanguagesBlock/LanguagesBlock';
import ToggleTheme from '../ToggleTheme/ToggleTheme';
import Button from '../Button/Button';
import { addMumbaiNetwork, importOnixToken } from '../../utils/web3';
import { useNetwork } from '../../hooks';
import { Theme } from '../../constants/general';


const DropdownSettings = () => {
  const { network: { isSupported } } = useNetwork();

  const [isOpen, setIsOpen] = useState(false);

  const { t } = useTranslation('common');
  const { theme } = useTheme();

  const isDark = theme === Theme.DARK;

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const icon = isOpen
    ? `/images/cross${isDark ? 'Dark' : ''}.svg`
    : `/images/settings${isDark ? 'Dark' : ''}.svg`;

  return (
    <div className="relative flex justify-center">
      <button
        type="button"
        className="cursor-pointer"
        onClick={toggleDropdown}
      >
        <Image
          src={icon}
          height={40}
          width={40}
          alt="settings"
        />
      </button>
      {isOpen && <ul
        className={`absolute z-[1000] top-[63px] right-[0px] flex-col flexCenter flex-wrap
          m-0 min-w-[210px] min-h-[220px] max-h-[270px] py-2 list-none overflow-hidden
          rounded-lg border-none bg-white bg-clip-padding text-left text-base
          shadow-xl border-1 dark:bg-neutral-700`}
      >
        {!isSupported && <li className="flex flex-1 flexCenter mx-2">
          <Button
            title={t('addMumbaiNetwork')}
            onClick={() => addMumbaiNetwork()}
          />
        </li>}
        <li className="flex flex-1 flexCenter mx-2">
          <Button
            title={t('addOnixToken')}
            onClick={() => importOnixToken()}
          />
        </li>
        <li className="flex flex-1 flexCenter">
          <LanguagesBlock />
        </li>
        <li className="flex flex-1 flexCenter">
          <ToggleTheme />
        </li>
      </ul>}
    </div>
  );
};

export default DropdownSettings;