import { useTranslation } from 'next-i18next';
import { useAccount, useNetwork } from '../../hooks';
import { getNetworkInfo } from '../../utils/general';

const NetworkStatus = () => {
  const { t } = useTranslation('common');
  const { account } = useAccount();
  const { network } = useNetwork();

  const { title, dotColor } = getNetworkInfo(network, account);

  return (
    <div className="flex items-center px-3 py-2 rounded-md dark:bg-dark-element bg-blue-500">
      <svg className="-ml-0.5 mr-1.5 h-2 w-2" fill={dotColor} viewBox="0 0 8 8">
        <circle cx={4} cy={4} r={3} />
      </svg>
      <span className="text-sm font-medium text-white">
        {t(title)}
      </span>
    </div>
  );
};

export default NetworkStatus;
