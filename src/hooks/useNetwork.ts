
import useSWR from 'swr';
import { useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { NetworkHookFactory } from '../interfaces/IHooks';
import { NETWORKS } from '../constants/general';

const targetId = process.env.NEXT_PUBLIC_TARGET_CHAIN_ID as string || "80001";
const targetNetwork = NETWORKS[targetId];

const createNetworkHook: NetworkHookFactory = ({ provider, isLoading }) => () => {
  const { t } = useTranslation('common');

  const { data, isValidating, mutate, ...swr } = useSWR(
    provider ? 'web3/useNetwork' : null,
    async () => {
      const { chainId } = await provider?.getNetwork() || {};

      if (!chainId) {
        toast.error(t('cannotRetrieveNetwork').toString());
      } else {
        return NETWORKS[chainId];
      }
    }, {
      revalidateOnFocus: false,
    },
  );

  const isSupported = data === targetNetwork;

  const handleChainChanged = (...args: Array<string>) => {
    if (args.length === 0) {
      toast.error(t('cannotRetrieveNetwork').toString());
    } else {
      const chainId = args[0].toString();
      mutate(NETWORKS[chainId]);
    }
  };

  useEffect(() => {
    ethereum?.on('chainChanged', handleChainChanged);
    return () => {
      ethereum?.removeListener('chainChanged', handleChainChanged);
    };
  }, []);

  return {
    ...swr,
    data,
    isValidating,
    targetNetwork,
    isSupported,
    mutate,
    isConnectedToNetwork: !isLoading && isSupported,
    isLoading: isLoading as boolean,
  };
};

export default createNetworkHook;
