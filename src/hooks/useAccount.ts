
import { useTranslation } from 'next-i18next';
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import useSWR from 'swr';
import { AccountHookFactory } from '../interfaces/IHooks';

const createAccountHook: AccountHookFactory = ({ provider, ethereum, isLoading }) => () => {
  const { t } = useTranslation('common');

  const { data, mutate, isValidating, ...swr } = useSWR(
    provider ? 'web3/useAccount' : null,
    async () => {
      const accounts = await provider?.listAccounts();
      const account = accounts?.[0];

      return account || null;
    }, {
      revalidateOnFocus: false,
      shouldRetryOnError: false,
    },
  );

  const handleAccountsChanged = (...args: unknown[]) => {
    const accounts = args[0] as string[];

    if (accounts[0] !== data) {
      mutate(accounts[0])
        .catch(() => {});
    }
  };

  useEffect(() => {
    ethereum?.on('accountsChanged', handleAccountsChanged);
    return () => {
      ethereum?.removeListener('accountsChanged', handleAccountsChanged);
    };
  });

  const connect = () => {
    ethereum?.request({ method: 'eth_requestAccounts' })
      .catch(() => {
        toast.error(t('somethingWentWrong').toString());
      });
  };

  return {
    ...swr,
    data,
    isValidating,
    isLoading: isLoading as boolean,
    isInstalled: ethereum?.isMetaMask || false,
    mutate,
    connect,
  };
};

export default createAccountHook;
