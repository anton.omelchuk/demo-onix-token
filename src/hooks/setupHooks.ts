import {
  UseAccountHook,
  UseNetworkHook,
  Web3Dependencies,
} from '../interfaces/IHooks';
import createAccountHook from './useAccount';
import createNetworkHook from './useNetwork';

export type Web3Hooks = {
  useAccount: UseAccountHook;
  useNetwork: UseNetworkHook;
};

export type SetupHooks = {
  (d: Web3Dependencies): Web3Hooks
};

export const setupHooks: SetupHooks = (deps) => ({
  useAccount: createAccountHook(deps),
  useNetwork: createNetworkHook(deps),
});
