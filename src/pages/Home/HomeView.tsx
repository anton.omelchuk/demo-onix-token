import { useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import Layout from '../../layout/Layout';
import Button from '../../components/Button/Button';
import PostsLists from '../../components/PostsList/PostsLists';
import { useWeb3 } from '../../providers/Web3Provider';

const HomeView = () => {
  const { t } = useTranslation('common');
  const { contracts } = useWeb3();

  const getTestTokens = useCallback(async () => {
    try {
      const { onixToken } = contracts || {};
      const tx = await onixToken?.freeMint();
      await tx?.wait();

      toast.success(t('tokensReceived').toString());
    } catch (error: unknown) {
      toast.error(error?.data?.message || error?.message || t('somethingWentWrong'));
    }
  }, [contracts, t]);

  return (
    <Layout>
      <PostsLists />
      <div className="fixed bottom-10 right-10">
        <Button title={t('getTestTokens')} onClick={getTestTokens} />
      </div>
    </Layout>
  );
};

export default HomeView;
