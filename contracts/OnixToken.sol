// SPDX-License-Identifier: UNLICENSED

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

pragma solidity 0.8.13;

contract Token is ERC20, Ownable {
    uint private _mintAmount = 10 * (10**18); // 10 tokens convert to wei
    mapping(address => bool) internal _hasMinted;

    constructor() ERC20("Onix Systems", "ONIX") {
        _mint(msg.sender, 100000 * (10**18));
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount * (10**18));
    }

    function freeMint() public {
        require(!_hasMinted[msg.sender], "Tokens can be minted only once per address.");

        _hasMinted[msg.sender] = true;

        _mint(msg.sender, _mintAmount);
    }
}
