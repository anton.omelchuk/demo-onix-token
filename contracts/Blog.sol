// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./OnixToken.sol";

contract Blog {
    OnixToken public token;
    address public owner;
    uint256 public postCost = 1 ether; // 1 ONIX
    uint256 public likeCost = 0.01 ether; // 0.01 ONIX

    struct Post {
        uint id;
        address author;
        string content;
        uint256 likes;
    }

   struct UserProfile {
        string name;
        string bio;
        string avatar;
        string backgroundImage;
    }

    Post[] public posts;
    mapping(address => UserProfile) public profiles;
    mapping(address => uint256) public pendingTokenBalances;

    event NewPost(uint256 indexed postId, address indexed author, string content, uint likes);
    event NewLike(uint256 indexed postId, address indexed liker);
    event PostDeleted(uint256 indexed postId, address indexed author);
    event ProfileCreated(address indexed user, string name, string bio, string avatar, string backgroundImage);
    event ProfileUpdated(address indexed user, string name, string bio, string avatar, string backgroundImage);
    event TokensWithdrawn(address indexed author, uint256 amount);

    constructor(address tokenAddress) {
        owner = msg.sender;
        token = OnixToken(tokenAddress);
    }

    modifier notEnoughTokens(uint amount) {
        require(token.balanceOf(msg.sender) >= amount, "Insufficient token balance");
        _;
    }

    modifier invalidPostId(uint postId) {
        require(postId < posts.length, "Invalid post ID.");
        _;
    }

    function createPost(string memory content) public payable notEnoughTokens(postCost) {
        // require(msg.value == postCost, "Please, send 1 ONIX to create post.");
        // token.transfer(owner, postCost);

        uint256 postId = posts.length;
        posts.push(Post(postId, msg.sender, content, 0));

        emit NewPost(postId, msg.sender, content, 0);
    }

    function likePost(uint256 postId) public payable invalidPostId(postId) notEnoughTokens(likeCost) {
        // require(msg.value == likeCost, "Please, send 0.01 ONIX to create post.");
        // token.transfer(owner, likeCost);

        posts[postId].likes++;
        pendingTokenBalances[posts[postId].author] += likeCost;

        emit NewLike(postId, msg.sender);
    }

    function deletePost(uint256 postId) public invalidPostId(postId) {
        require(posts[postId].author == msg.sender, "Only the author can delete their own post.");

        if (postId < posts.length - 1) {
            posts[postId] = posts[posts.length - 1];
        }
        posts.pop();

        emit PostDeleted(postId, msg.sender);
    }

    function getAllPosts() public view returns (Post[] memory) {
        return posts;
    }

    function getPostsByAuthor(address author) public view returns (Post[] memory) {
        uint256 count = 0;
        for (uint256 i = 0; i < posts.length; i++) {
            if (posts[i].author == author) {
                count++;
            }
        }

        Post[] memory authorPosts = new Post[](count);
        uint256 index = 0;
        for (uint256 i = 0; i < posts.length; i++) {
            if (posts[i].author == author) {
                authorPosts[index++] = posts[i];
            }
        }

        return authorPosts;
    }

    function withdrawTokens() public {
        uint256 amount = pendingTokenBalances[msg.sender];
        require(amount > 0, "No tokens available to withdraw.");

        pendingTokenBalances[msg.sender] = 0;
        // token._transfer(owner, msg.sender, amount);

        emit TokensWithdrawn(msg.sender, amount);
    }

    // User profile

    function createProfile(
        string memory name,
        string memory bio,
        string memory avatar,
        string memory backgroundImage
    ) public {
        require(bytes(profiles[msg.sender].name).length == 0, "Profile already exists.");

        profiles[msg.sender] = UserProfile(name, bio, avatar, backgroundImage);

        emit ProfileCreated(msg.sender, name, bio, avatar, backgroundImage);
    }

    function updateProfile(
        string memory name,
        string memory bio,
        string memory avatar,
        string memory backgroundImage
    ) public {
        require(bytes(profiles[msg.sender].name).length != 0, "Profile not found.");

        profiles[msg.sender] = UserProfile(name, bio, avatar, backgroundImage);

        emit ProfileUpdated(msg.sender, name, bio, avatar, backgroundImage);
    }
}
