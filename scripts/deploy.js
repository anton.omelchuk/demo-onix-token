const fs = require('fs');
const mkdirp = require('mkdirp');
const hre = require('hardhat');

async function main() {
  const OnixToken = await hre.ethers.getContractFactory('OnixToken');
  const onixToken = await OnixToken.deploy();
  await onixToken.deployed();

  const Blog = await hre.ethers.getContractFactory('Blog');
  const blog = await Blog.deploy(onixToken.address);
  await blog.deployed();

  console.log('OnixToken deployed to:', onixToken.address);
  console.log('Blog deployed to:', blog.address);

  const data = {
    OnixToken: onixToken.address,
    Blog: blog.address
  };

  const addressData = JSON.stringify(data, null, 2);

  const dirPath = './public/artifacts';

  if (!fs.existsSync(dirPath)) {
    mkdirp.sync(dirPath);
  }

  fs.writeFileSync('./public/artifacts/address.json', addressData);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
