FROM node:18.7

ENV PORT 3000

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

ENV MUMBAI_RPC_URL "https://polygon-mumbai.g.alchemy.com/v2/oJT8rJ9csOjmfHSgAPNxfhHoXlN5sEdC"
ENV WALLET_PRIVATE_KEY "abd1488dbc705485783b331bc407d2e4eb5076de415ec8435eb742bfbdacec1a"
ENV NEXT_PUBLIC_NETWORK_ID "5777"
ENV NEXT_PUBLIC_TARGET_CHAIN_ID "80001"

# install app dependencies
COPY package*.json /usr/src/app/
RUN npm install

# add app
COPY . /usr/src/app

RUN npm run build

EXPOSE 3000

# start app
CMD ["npm", "start"]
