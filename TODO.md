# Smart contracts
# Onix Token
(1)
√ Create token interface with common functionality
√ Create instance of token
√ Allow each user to mint free test token (only once)
√ Burn token
√ Mint new tokens by contract owner
(2)
- Add token logo
# BLOG
√ Add new post
√ Add likes to post
√ Withdraw earned tokens
(2)
- Create user profile
- Edit user profile
- Add tests

# Fronted
- Stack: Next, TypeScript, Redux, <h6>Eslint</h6>, Tailwind, Hardhat
# Features
(1)
√ Toggle language
√ Toggle theme
√ Connect Wallet
√ Show current network
√ Change Wallet Account listener
√ Change Network listener
√ Configure deploy to localhost
√ Configure deploy to testnet
√ Mint token button
√ Add new post at app side
√ Like post at app side
- Withdraw earned tokens at app side
(2)
- Show wallet balance
- Responsive
√ Button -> Add Polygon Mumbai chain to MM
√ Button -> Add Onix Token to MM
- Link to faucet MATIC in Mumbai
# UI
√ Navbar with logo, profile avatar (when wallet is connected)
√ Network status with
√ Textarea to create new post
√ List all tweets
√ Each tweet contains only text and likes count, like button
(2)
- Guide page
- Profile page (name, bio, background, available tokens to withdraw)
- Footer with Onix info (website, social networks)
- Settings page (lang, theme)