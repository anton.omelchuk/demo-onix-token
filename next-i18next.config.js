module.exports = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'ua'],
  },
  react: { useSuspense: false }
};
