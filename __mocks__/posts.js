export default [
  {
    id: 1,
    author: 'John Doe',
    likes: 0,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 2,
    author: 'Jane E.',
    likes: 9,
    content: 'Sed euismod, nunc. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
  {
    id: 3,
    author: 'John Doe',
    likes: 3,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 4,
    author: 'John Doe',
    likes: 2,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 5,
    author: 'John Doe',
    likes: 17,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 6,
    author: 'John Doe',
    likes: 10,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 7,
    author: 'John Doe',
    likes: 30,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 8,
    author: 'John Doe',
    likes: 34,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 9,
    author: 'John Doe',
    likes: 18,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 10,
    author: 'John Doe',
    likes: 21,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 11,
    author: 'John Doe',
    likes: 11,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
  {
    id: 12,
    author: 'John Doe',
    likes: 7,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nunc.',
  },
]