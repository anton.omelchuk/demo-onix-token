import { MetaMaskInpageProvider } from '@metamask/providers';

declare global {
  interface Window {
    ethereum: MetaMaskInpageProvider;
  }
}

export interface IError {
  message?: string;
  data?: {
    message?: string;
  };
}
