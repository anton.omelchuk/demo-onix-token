require("@nomicfoundation/hardhat-toolbox");
require("dotenv").config();

const { MUMBAI_RPC_URL, WALLET_PRIVATE_KEY } = process.env;

module.exports = {
  solidity: "0.8.19",
  defaultNetwork: 'hardhat',
  networks: {
    hardhat: {},
    mumbai: {
      url: MUMBAI_RPC_URL,
      accounts: [`0x${WALLET_PRIVATE_KEY}`],
    },
  },
  paths: {
    artifacts: './public/artifacts',
  },
};
