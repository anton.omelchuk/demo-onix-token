# Next.js + Jest

This example shows how to configure Jest to work with Next.js.

This includes Next.js' built-in support for Global CSS, CSS Modules, and TypeScript!

## How to Use
- To run Next.js project: npm run dev;
- To deploy contracts locally: npm run deploy:localhost;
- To deploy contract to testnet (Polygon): npm run deploy;

- To start Next.js project and connect to contracts:
  - first terminal tab: npx hardhat node
  - second terminal tab: npm run web3:dev

This command deploys contracts to local network,
create file with contracts address and
connect application to contracts by using these addresses
and contracts abi.
